#if !defined(REVERSE_STRING_H)
#define REVERSE_STRING_H

#include <string>

namespace reverse_string {
  std::string reverse_string(std::string s);
}  // namespace reverse_string
void swap(char &a, char &b);

#endif // REVERSE_STRING_H
