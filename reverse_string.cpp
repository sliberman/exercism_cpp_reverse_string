#include "reverse_string.h"
#include <iostream>

void swap(char &a, char &b) {
  char tmp = a;
  a = b;
  b = tmp;
}

namespace reverse_string {
  std::string reverse_string(std::string s) {
    for (auto c = std::make_pair(s.begin(), s.rbegin());
	 c.first < s.end() - s.length()/2;
	 ++c.first, ++c.second) {
      swap(*c.first, *c.second);
    }
    return s;
  }
}  // namespace reverse_string
